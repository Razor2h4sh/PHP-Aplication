<div class="container">
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<div class="form form-top">
				<fieldset>
					<legend class="col-form-legend">Calculator</legend>
					<p><label class="sr-only">Data 1</label><input class="form form-control" type="text" name="data1" placeholder="Data 1" autocomplete="off" id="data1"></p>
					<p><label class="sr-only">Data 2</label><input class="form form-control" type="text" name="data2" placeholder="Data 2" autocomplete="off" id="data2"></p>
					<p><select class="form form-control" name="action" id="actionc">
						<option value="----">--action--</option>
						<option value="Addition">Addition</option>
						<option value="Subtraction">Subtraction</option>
						<option value="Multiplication">Multiplication</option>
						<option value="Division">Division</option>
						<option value="Modulus">Modulus</option>
						<option value="Exponentiation">Exponentiation</option>
					</select></p>
					<p><input class="btn btn-block btn-success" type="submit" name="hitung" value="equals" onclick="calculator()"></p>
					<p><input class="form form-control" type="text" name="hasil" value="Equals" disabled id="equals"></p>
				</fieldset>
			</div>
		</div>
		<div class="col-sm-4"></div>
	</div>
</div>