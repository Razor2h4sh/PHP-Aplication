<div class="container" style="margin-top: 20%;">
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<div class="alert alert-danger" role="alert">
				<p align="center"><i class="fa fa-exclamation-triangle" style="font-size:100px;"></i></p>
				<p align="center">Error there no item named
					<?php
					if (isset($_GET['action'])) {
						echo $_GET['action'];
					}
					if (isset($_GET['menu'])) {
						echo $_GET['menu'];
					}
					?></p>
			</div>
		</div>
		<div class="col-sm-4"></div>
	</div>
</div>