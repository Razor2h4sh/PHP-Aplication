<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="Muhammad Aviv Burhanudin">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP Aplication</title>
	<link rel="icon" href="S.jpg">
	<link rel="stylesheet" href="asset/bootstrap-4.0.0-dist/css/bootstrap.css">
	<link rel="stylesheet" href="asset/font-awesome-4.7.0/css/font-awesome.css">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<?php include"sql.php"; ?>
	<div id="navbar">
		<?php include"navbar.php"; ?>
	</div>
	<div id="ajax">
		<?php include"home.php"; ?>
	</div>
	<script type="text/javascript" src="asset/bootstrap-4.0.0-dist/popper.min.js"></script>
	<script type="text/javascript" src="asset/bootstrap-4.0.0-dist/jquery-3.2.1.slim.min.js"></script>
	<script type="text/javascript" src="asset/bootstrap-4.0.0-dist/js/bootstrap.js"></script>
	<script type="text/javascript" src="script.js" charset="utf-8" async defer></script>
</body>
</html>