<nav class="navbar navbar-expand-lg navbar-dark bg-dark text-white">
	<ul class="navbar-nav">
		<li class="nav-item">
			<a class="nav-link" href="javaScript:void(0)" title="Status">Status : <span id="loading" style="display: none;"><i class="fa fa-refresh fa-spin"></i> Loading...</span> <span id="success"><i class="fa fa-check"></i> Success</span></a>
		</li>
	</ul>
</nav>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark text-white">
	<a class="navbar-brand" href="<?php $_SERVER['PHP_SELF']; ?>"><i class="fa fa-home"></i> PHP Aplication >></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#show"
	aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="show">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<a id="aboutus" class="nav-link" href="javaScript:void(0)" onclick="domChange('aboutus.php', 'GET', 'ajax',);"><i class="fa fa-info"></i> About</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="javaScript:document.location.reload()" title="Reload page"><i class="fa fa-refresh"></i> Reload</a>
			</li>
			<?php
			if (!empty($_SESSION['level'])) { ?>
			<li class="nav-item">
				<a class="nav-link" href="javaScript:void(0)" onclick="domChange('calculator.php', 'GET', 'ajax',);"><i class="fa fa-calculator"></i> Calculator</a>
			</li>
			<?php
			}
			?>
			<?php if (!empty($_SESSION['level']) && $_SESSION['level'] == 1) { ?>
			<li class="nav-item">
				<a class="nav-link" href="javaScript:void(0)" onclick="domChange('user.php', 'GET', 'ajax',);"><i class="fa fa-group"></i> User</a>
			</li>
			<?php
			}
			?>
		</ul>
		<!--<form class="form-inline my-2 my-lg-0">
				<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		</form>-->
		<?php
		if (empty($_SESSION['level'])) { ?>
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" href="javaScript:void(0)" onclick="domChange('signin.php', 'GET', 'ajax',)"><i class="fa fa-sign-in"></i> Sign in</a>
			</li>
		</ul>
		<?php
		}
		if (!empty($_SESSION['level'])) { ?>
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" href="javaScript:void(0)" onclick="signout()" id="action"><i class="fa fa-sign-out"></i> Sign out</a>
			</li>
		</ul>
		<?php
		}
		?>
	</div>
</nav>