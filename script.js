function domChange(file, methode, id, Async=true) {
	var load = document.getElementById("loading");
	var success = document.getElementById("success");
	load.style.display = "";
	success.style.display = "none";
	let xml = new XMLHttpRequest();
	setTimeout(function(){
			xml.open(methode, file, Async);
			xml.onreadystatechange = function() { 
			if (this.readyState == 4 && this.status == 200) { 
				document.getElementById(id).innerHTML = this.responseText;
				load.style.display = "none";
				success.style.display = "";
			}
		};
		xml.send();
	},200);
}
function signup() {
	let name = document.getElementById("name").value;
	let username = document.getElementById("username").value;
	let email = document.getElementById("email").value;
	let password = document.getElementById("password").value;
	let level = document.getElementById("level").value;
	let action = document.getElementById("action").value;
	var load = document.getElementById("loading");
	var success = document.getElementById("success");
	load.style.display = "";
	success.style.display = "none";
	let xml = new XMLHttpRequest();
	setTimeout(function(){
		xml.open("POST", "serverIO.php", true);
		xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xml.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById("signup").innerHTML = this.responseText;
				load.style.display = "none";
				success.style.display = "";
			}
		};
		xml.send("action="+action+"&name="+name+"&username="+username+"&email="+email+"&level="+level+"&password="+password);
	},200);
}
function signin() {
	let username = document.getElementById("username").value;
	let password = document.getElementById("password").value;
	let action = document.getElementById("action").value;
	var load = document.getElementById("loading");
	var success = document.getElementById("success");
	load.style.display = "";
	success.style.display = "none";
	let xml = new XMLHttpRequest();
	setTimeout(function(){
		xml.open("POST", "serverIO.php", true);
		xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xml.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById("signin").innerHTML = this.responseText;
				load.style.display = "none";
				success.style.display = "";
			}
		};
		xml.send("action="+action+"&username="+username+"&password="+password);
	},200);
}
function signout() {
	var load = document.getElementById("loading");
	var success = document.getElementById("success");
	load.style.display = "";
	success.style.display = "none";
	let xml = new XMLHttpRequest();
	setTimeout(function(){
			xml.open("POST", "serverIO.php", true);
			xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xml.onreadystatechange = function() { 
			if (this.readyState == 4 && this.status == 200) {
				load.style.display = "none";
				success.style.display = "";
				document.location.reload()
			}
		};
		xml.send("action=signout");
	},200);
}
function calculator() {
	let dataOne = parseInt(document.getElementById("data1").value);
	let dataTwo = parseInt(document.getElementById("data2").value);
	let actionc = document.getElementById("actionc").value;
	let equals = document.getElementById("equals");
	switch (actionc) {
		case "Addition":
			equals.value = dataOne + dataTwo;
			break;
		case "Subtraction":
			equals.value = dataOne - dataTwo;
			break;
		case "Multiplication":
			equals.value = dataOne * dataTwo;
			break;
		case "Division":
			equals.value = dataOne / dataTwo;
			break;
		case "Modulus":
			equals.value = dataOne % dataTwo;
			break;
		case "Exponentiation":
			equals.value = Math.pow(dataOne, dataTwo);
			break;
		case "----":
			equals.value = "Chose Action Above";
			break;
		default:
			equals.value = "Equals";
			break;
	}
}
function searchUser() {
	let val = document.getElementById("look").value;
	let xml = new XMLHttpRequest();
	setTimeout(function(){
			xml.open("POST", "serverUser.php", true);
			xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xml.onreadystatechange = function() { 
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById("user").innerHTML = this.responseText;
			}
		};
		xml.send("action=searchUser&value="+val);
	},200);
}
function addUser() {
	let name = document.getElementById("name").value;
	let username = document.getElementById("username").value;
	let email = document.getElementById("email").value;
	let password = document.getElementById("password").value;
	let level = document.getElementById("level").value;
	let action = "Add";
	var load = document.getElementById("loading");
	var success = document.getElementById("success");
	load.style.display = "";
	success.style.display = "none";
	let xml = new XMLHttpRequest();
	setTimeout(function(){
		xml.open("POST", "serverUser.php", true);
		xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xml.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById("user").innerHTML = this.responseText;
				load.style.display = "none";
				success.style.display = "";
			}
		};
		xml.send("action="+action+"&name="+name+"&username="+username+"&email="+email+"&level="+level+"&password="+password);
	},200);
}
function editUser(id) {
	let action = "edit";
	var load = document.getElementById("loading");
	var success = document.getElementById("success");
	load.style.display = "";
	success.style.display = "none";
	let xml = new XMLHttpRequest();
	setTimeout(function(){
		xml.open("POST", "serverUser.php", true);
		xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xml.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById("ressultEdt").innerHTML = this.responseText;
				load.style.display = "none";
				success.style.display = "";
			}
		};
		xml.send("action="+action+"&id="+id);
	},200);
}
function deleteUser(id) {
	let action = "delete";
	var load = document.getElementById("loading");
	var success = document.getElementById("success");
	load.style.display = "";
	success.style.display = "none";
	let xml = new XMLHttpRequest();
	setTimeout(function(){
		xml.open("POST", "serverUser.php", true);
		xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xml.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById("user").innerHTML = this.responseText;
				load.style.display = "none";
				success.style.display = "";
			}
		};
		xml.send("action="+action+"&id="+id);
	},200);
}
/*function viewUser() {
	let value = document.getElementById("view").value;
	var load = document.getElementById("loading");
	var success = document.getElementById("success");
	load.style.display = "";
	success.style.display = "none";
	let xml = new XMLHttpRequest();
	setTimeout(function(){
		xml.open("POST", "serverUser.php", true);
		xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xml.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById("action").innerHTML = this.responseText;
				load.style.display = "none";
				success.style.display = "";
			}
		};
		xml.send(value);
	},200);
}
*/
function passwordTogler(id, icon, toggleText) {
	let x = document.getElementById(id);
	let y = document.getElementById(icon);
	let z = document.getElementById(toggleText);
	if (x.type == "password") {
		y.classList.remove("fa-eye")
		y.classList.add("fa-eye-slash")
		z.innerHTML = "Hide ";
		x.type = "text";
	} else {
		y.classList.remove("fa-eye-slash")
		y.classList.add("fa-eye")
		z.innerHTML = "Show ";
		x.type = "password";
	}
}
function saveUserEdit() {
	let id = parseInt(document.getElementById("saveid").value);
	let name = document.getElementById("savename").value;
	let username = document.getElementById("saveusername").value;
	let email = document.getElementById("saveemail").value;
	let action = "saveUserEdit";
	var load = document.getElementById("loading");
	var success = document.getElementById("success");
	load.style.display = "";
	success.style.display = "none";
	let xml = new XMLHttpRequest();
	setTimeout(function(){
		xml.open("POST", "serverUser.php", true);
		xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xml.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById("user").innerHTML = this.responseText;
				load.style.display = "none";
				success.style.display = "";
			}
		};
		xml.send("action=saveUserEdit&id="+id+"&name="+name+"&username="+username+"&email="+email);
	},200);
}
var header = document.getElementById("show");
var btns = header.getElementsByClassName("nav-link");
for (var i = 0; i < btns.length; i++) {
	btns[i].addEventListener("click", function() {
	var current = document.getElementsByClassName("active");
	current[0].className = current[0].className.replace(" active", "");
	this.className += " active";
  });
}
