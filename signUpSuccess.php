<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Success</title>
		<link rel="stylesheet" href="asset/bootstrap-4.0.0-dist/css/bootstrap.css">
		<link rel="stylesheet" href="asset/font-awesome-4.7.0/css/font-awesome.css">
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="jumbotron">
					<h1 class="display-4">Signup Success</h1>
					<p class="lead">Wellcome to PHP Aplication</p>
					<p class="lead">
						<a class="btn btn-primary btn-lg" href="#" role="button">Signin</a>
					</p>
				</div>
			</div>
		</div>
		<script src="asset/bootstrap-4.0.0-dist/popper.min.js" type="text/javascript" charset="utf-8" async defer></script>
		<script src="asset/bootstrap-4.0.0-dist/jquery-3.2.1.slim.min.js" type="text/javascript" charset="utf-8" async defer></script>
		<script src="asset/bootstrap-4.0.0-dist/js/bootstrap.js" type="text/javascript" charset="utf-8" async defer></script>
	</body>
</html>
<?php
?>