<div class="container">
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<div class="form-top">
				<fieldset>
					<legend class="col-form-legend">Signup</legend>
					<p><label class="sr-only col-form-label">Name</label><input class="form form-control" type="text" name="name" placeholder="Name" id="name"></p>
					<p><label class="sr-only col-form-label">Username</label><input class="form form-control" type="text" name="user" placeholder="Username" id="username"></p>
					<p><label class="sr-only col-form-label">Email</label><input class="form form-control" type="email" name="email" placeholder="Email" id="email"></p>
					<p><label class="sr-only col-form-label">Password</label><input class="form form-control" type="password" name="password" placeholder="password" id="password"></p>
					<p><label class="sr-only col-form-label">Confirm Password</label><input class="form form-control" type="password" name="repassword" placeholder="Confirm Password" id="repassword"></p>
					<p><input type="text" name="level" value="2" id="level" hidden disabled></p>
					<p><input class="btn btn-info btn-block" type="submit" name="daftar" value="signup" id="action" onclick="signup();"></p>
					<p>have acount ? <a href="javaScript:void(0)" title="singup" onclick="domChange('signin.php', 'GET', 'ajax')">Signin</a></p>
				</fieldset>
			</div>
			<div id="signup">
			</div>
		</div>
		<div class="col-sm-4"></div>
	</div>
</div>