<div class="container">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<div class="form-control">
				<input class="form-control" type="text" placeholder="Search Name or Username" name="look" id="look" onkeyup="searchUser();">
			</div>
			<div class="form-inline form-control" style="margin-top: 2%;margin-bottom: 2%;">
				<select class="form-control mr-sm-1" name="level" id="level">
					<option value="1">Admin</option>
					<option value="2">User</option>
				</select>
				<input class="form-control mr-sm-1" type="text" name="name" placeholder="Name" id="name">
				<input class="form-control mr-sm-1" type="text" name="user" placeholder="Username" id="username">
				<input class="form-control mr-sm-1" type="text" name="email" placeholder="Email" id="email">
				<input class="form-control mr-sm-1" type="text" name="password" placeholder="Password" id="password">
				<span class="mr-sm-5"></span>
				<input class="btn btn-success btn-lg" type="submit" name="daftar" value="Add" id="action" onclick="addUser()">
			</div>
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			      	<div id="ressultEdt"><!--Result of edit goes here--></div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <button onmousedown="saveUserEdit()" data-dismiss="modal" type="button" class="btn btn-primary">Save changes</button>
			      </div>
			    </div>
			  </div>
			</div>
			<div id="user">
			<?php
			include"sql.php";
			$command = "SELECT * FROM user";
			$query = mysqli_query($connect, $command);
			$no = 0;
			?>
			<table class="table table-responsive">
				<tr>
					<th>No</th>
					<th>Name</th>
					<th>Username</th>
					<th>Acount Create On</th>
					<th>Last Login</th>
					<th>Email</th>
					<th>Privilege</th>
					<th>Action</th>
				</tr>
				<?php while ($data = mysqli_fetch_array($query)) { 
					$no++; 
					$levelx = $data['level'];
					switch ($levelx) {
					case '1':
						$levelu = '<p align="center"><i class="fa fa-circle" style="color:red;"></i></p>';
						break;
					case '2':
						$levelu = '<p align="center"><i class="fa fa-circle" style="color:green;"></i></p>';
						break;
						}
					?>
				<tr>
					<td><?php echo $no; ?></td>
					<td><?php echo $data['name']; ?></td>
					<td><?php echo $data['username']; ?></td>
					<td><?php echo $data['AcountCreateOn']; ?></td>
					<td><?php echo $data['lastlogin']; ?></td>
					<td><?php echo $data['email']; ?></td>
					<td><?php echo $levelu; ?></td>
					<td>
						<?php if ($levelx == 1) { ?>
							Admin <i class="fa fa-exclamation-triangle" style="color: red;"></i>
						 <?php } else { ?>
							<button onmouseenter="editUser(<?php echo $data['id'] ?>)" id="edit" type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal" value="id=<?php echo $data['id']?>"><i class="fa fa-edit"></i> Edit</button>
							<button onclick="deleteUser(<?php echo $data['id'] ?>)" type="button" class="btn btn-danger"><i class="fa fa-eraser"></i> Delete</button>
						<?php } ?>
					</td>
				</tr>
				<?php } ?>
			</table>
			<?php
			?>
		</div>
		</div>
		<div class="col-sm-1"></div>
	</div>
</div>